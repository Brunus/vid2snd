# changelog :

- uncompiler received the album mode to deal with full albums by one artist
- uncompiler received support for unicode filenames
- My Leeping Karma - Soma album playlist added as demo for the album mode

# vid2snd
vid2snd is a small Python script to help with dowloading video soundtracks from Youtube, Dailymotion etc, encoding it in OGG or MP3 and adding tags.

Requirements : Python, ffmpeg, youtube-dl (Python lib), mutagen (Python lib).

usage: vid2snd.py [-h] -u URL [-c CODEC] [-br BITRATE] [-A ARTIST] [-a ALBUM] [-t TITLE] [-tn TRACKNUMBER] [-y YEAR] [-g GENRE] [-v VERSION]

Available codecs are mp3 and ogg(vorbis).

While I use this script to download live performances soundtracks, I use the "version" tag to store informations on the location and date.

Only the url is required, you can let empty all tags but I advise to fill some tags to keep your collection easely browsable.

Encoding rate is by default 192Kbs, but it will be added to parameters available in the options.

Ex :
vid2snd.py -u https://www.youtube.com/watch?v=AXh1Oxx5MYs -c ogg -br 256 -a "The Go! Team" -A "Proof of youth" -t "Keys to the city" -y 2016 -g "pop rock" -v "England, Hebden Bridge 26/08/2016" -tn 04

# uncompiler
uncompiler is a small script to extract and tag tracks from a "Full album" or a compile downloaded from YouTube with either vid2snd or youtube-dl.

Requirements : Python, ffmpeg, mutagen (Python lib), a playlist with timecodes.

The playlist has to have the same structure as the one provided as demo : compile-stoner-volume-1-by-FuzzFM.txt

This play list is associated file the file doawnload from this YouTube vid : https://www.youtube.com/watch?v=hjqk2vlkAno

Right now, just copy/paste the YouTube Page playlist is not enough, you have to structure it like in the demo file, but I'll improve this asap.

usage :

uncompiler.py uncompiler.py [-h] -i INPUTFILE -p PLAYLIST [-A ARTIST] [-a ALBUM] [-g GENRE]

Note : for a compilation with several artists (you don't need to provide artist's name):

Ex : You downloaded the audio track of this video : https://youtu.be/u9GnGMQz22A  
As I'm providing a better timeline than the one provided on the YouTube Page, use it ! ;D

uncompiler.py -i The-Specials_30th-aniversary-tour.mp3 -p TheSpecials_30th_AnniversaryTour.txt -A "The Special" -a "30th Aniversary Tour" -g "Ska"

It will export as many files as there is tracks in the playlist, taged with artists names, tracks titles, albums names and genres.

# To do :

- Providing a quiet mode and verbose mode for uncompiler
- Providing an album mode with compile support for full albums with various artists
- Building play lists directly from youtube pages from (not sure to provide this one day...)



