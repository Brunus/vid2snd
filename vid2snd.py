#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Author : Brunus-V
Licence : MIT
Version : 0.1.2
'''

from __future__ import unicode_literals
import sys
import os
import argparse
from Queue import Queue

try:
    import youtube_dl
except:
    print('vid2snd needs youtube-dl library to be installed : https://rg3.github.io/youtube-dl/download.html')
    sys.exit()

parser = argparse.ArgumentParser(
    description='Vid2snd downloads a video from Youtube, Dailymotion, etc, encode it in MP3 or OGG and makes you able to add tags.')
parser.add_argument('-u', '--url', help='url', required=True)
parser.add_argument(
    '-c', '--codec', help='codec, must be mp3 or vorbis (ogg) (default: %(default)s)', default="mp3", required=False)
parser.add_argument('-br', '--bitrate', help='bitrate (default: %(default)s)',
                    default="192", required=False)
parser.add_argument('-A', '--artist', help='artist', required=False)
parser.add_argument('-a', '--album', help='album', required=False)
parser.add_argument('-t', '--title', help='title', required=False)
parser.add_argument('-tn', '--tracknumber',
                    help='track number, on two digits : 01, 07, or 12', required=False)
parser.add_argument('-y', '--year', help='year', required=False)
parser.add_argument('-g', '--genre', help='genre', required=False)
parser.add_argument('-v', '--version',
                    help='other infos like location, country, city, etc', required=False)
parser.add_argument('-pl', '--playlist', help='add to playlist', required=False)

# to convert directly in unicode in the paser dict, you add type=lambda s:
# unicode(s, 'utf8') for each arg

args = parser.parse_args()

mutagenWarning = "vid2snd needs mutagen library to be installed : https://mutagen.readthedocs.io/en/latest/"

# initiating videao url
vid_url = args.url

# initiating queue to record the hook callback function datas and use it in main fuction
bus = Queue()

# initiating codec and importing right module from mutagen
if args.codec == "mp3":
    try:
        from mutagen.easyid3 import EasyID3
        codec = "mp3"
    except:
        print mutagenWarning
        sys.exit()
elif args.codec == "vorbis" or args.codec == "ogg":
    try:
        from mutagen.oggvorbis import OggVorbis
        codec = "vorbis"
    except:
        print mutagenWarning
        sys.exit()
else:
    print "unknow codec"
    sys.exit()


# Track number control
def track_number_control(track):

    # Allowed chars in track number
    ok = "0123456789"

    if track.__len__() != 2 or all(c in ok for c in track) == False:
        return False
    else:
        return track


# The tag function
def tag(f, codec, track):

    if codec == "mp3":
        audiofile = EasyID3(f)
    else:
        audiofile = OggVorbis(f)

    print("tagging file...")

    if(args.artist):
        audiofile['artist'] = unicode(args.artist, 'utf8')
    if(args.album):
        audiofile['album'] = unicode(args.album, 'utf8')
    if(args.title):
        audiofile['title'] = unicode(args.title, 'utf8')
    if(args.year):
        audiofile['date'] = args.year
    if(args.genre):
        audiofile['genre'] = args.genre
    if(args.version):
        audiofile['version'] = unicode(args.version, 'utf8')
    if(track != False):
        audiofile['tracknumber'] = track
    audiofile.save()
    print("done...")


# Quick'n dirty duration calculation using mutagen info extractor
# This function needs mor work, the mutagen info extractor don't return the right length
# while it's based on a VBR, then it's only an estimation...
def calculate_duration(codec, filename):
    if codec == "mp3":
        audiofile = EasyID3(filename)
    else:
        audiofile = OggVorbis(filename)
    return str(round(audiofile.info.length))


def addto_playlist(playlist, duration):
    title_line = "#EXTINF:" + duration + "," + args.artist + " - " + args.title + "\n"
    with open(playlist, "a+") as filename:
        filename.write(title_line)
    return()


# Hook callback function is used to display progression of downloads
def hook(s):
        # recording datas in the queue
    bus.put(s)
    # displaying progression for doawnload state
    if s['status'] == "downloading":
        sys.stdout.write('\r')
        sys.stdout.write('download progression : ' + str(s['_percent_str']))
        if s['_percent_str'] == "100.0%":
            sys.stdout.write('\n' + 'encoding...' + '\n')
        sys.stdout.flush()


# Download function, calling youtube-dl and taht also initiate encoding
def download(url, codec):
    ydl_options = {
        'outtmpl': '%(title)s.%(ext)s',
        'format': 'bestaudio',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': codec,
            'preferredquality': args.bitrate,
        }],
        'continuedl': True,
        'quiet': True,
        'restrictfilenames': True,
    }
    with youtube_dl.YoutubeDL(ydl_options) as ydl:

        try:
            # did not find it usefull for now
            # ydl.add_default_info_extractors()
            ydl.add_progress_hook(hook)
            info = ydl.extract_info(url, download=True)
            print(info['title'])
            # recovering result's datas in the queue and return it to main function
            result = bus.get()
            return result

        except (youtube_dl.utils.DownloadError, youtube_dl.utils.ContentTooShortError, youtube_dl.utils.ExtractorError) as e:
            print(e)


if __name__ == "__main__":

    if args.tracknumber:
        track = track_number_control(args.tracknumber)
        if track == False:
            print('error : track number must be composed only by couple of digits like 01, 03, 12, 35...')
            sys.exit()
    else:
        track = False
    print("initiating download...")
    result = download(vid_url, codec)

    if result:
        filename = result['filename']
        filename = filename[:-4]
        if not filename.endswith('.'):
            filename = filename + '.'
        if codec == "mp3":
            filename = filename + 'mp3'
        else:
            filename = filename + 'ogg'
        print(filename)

        # calling tagging function
        tag(filename, codec, track)

        # calling playlist function
        if args.playlist:

            # creating playlist file if it don't exist
            if not os.path.isfile(args.playlist):
                with open(args.playlist, 'w') as playlist:
                    playlist.write("#EXTM3U \n")

            # calculating duration for the current audio file
            duration = calculate_duration(codec, filename)

            # adding title to playlist
            addto_playlist(args.playlist, duration)

            # todo
